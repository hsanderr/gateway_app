# -*- encoding: utf-8 -*-


def date_diff_in_seconds(first, last):
    delta = last - first
    return delta.days * 24 * 3600 + delta.seconds
