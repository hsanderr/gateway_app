# -*- encoding: utf-8 -*-


def two_complement_convert(val, bits):
    if (val & (1 << (bits - 1))) != 0:
        val = val - (1 << bits)
    return val
