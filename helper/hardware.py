#! -*- encoding: utf-8 -*-
# sys libs
from logging import getLogger
from getmac import get_mac_address

def get_hardware_mac(ifname='eth0'):
    """
     Função para retornar o MAC da interface conectada a internet
     :return:
    """
    try:
        return get_mac_address(interface="eth0")
    except Exception as error:
        getLogger().error('Error reading gateway mac. {}'.format(error))
        return None