# -*- encoding: utf-8 -*-

import logging
import os
import zipfile
from threading import Thread

import requests

import app_settings
import helper.network.connection as connection


class WorkerUpdater(Thread):

    def __init__(self):
        Thread.__init__(self)

    def run(self) -> None:
        id_version_file = '1xizvJTXCnQbK6gUlbU3xfN9uEgammJv1'
        id_app_file = '1eJzQGIwjjD8ZpfOgKD8hIquvwFKp4qJI'
        destination = os.path.join(app_settings.ROOT_DIR, 'gateway_version.txt')

        try:
            if connection.internet_on():
                self._download_file_from_google_drive(id_version_file, destination)
                with open(destination, 'r') as textfile:
                    line = textfile.readline().strip()
                    if line > app_settings.APP_VERSION:
                        download_new_app = True
                    else:
                        download_new_app = False
                    os.remove(destination)
        except Exception as error:
            logging.getLogger().warning('Erro ao tentar verificar nova versão do software. Erro: {}'.format(error))

        if download_new_app:
            destination = os.path.join(app_settings.ROOT_DIR, 'gateway_app.zip')
            try:
                if connection.internet_on():
                    self._download_file_from_google_drive(id_app_file, destination)
                    with zipfile.ZipFile(destination, 'r') as myzipfile:
                        myzipfile.extractall(app_settings.ROOT_DIR)

                    os.remove(destination)
                    new_version_available = True
            except Exception as error:
                logging.getLogger().warning('Erro ao tentar obter nova versão do software. Erro: {}'.format(error))
                new_version_available = False

            if new_version_available:
                logging.getLogger().warning('Nova versão do software instalada: {}\nRebooting...'.format(line))
                os.system('sudo reboot now')
        else:
            logging.getLogger().info('Versão atual do aplicativo é a mais recente: {}'.format(app_settings.APP_VERSION))

    def _download_file_from_google_drive(self, id, destination):
        URL = "https://docs.google.com/uc?export=download"

        session = requests.Session()

        response = session.get(URL, params={'id': id}, stream=True)
        token = self._get_confirm_token(response)

        if token:
            params = {'id': id, 'confirm': token}
            response = session.get(URL, params=params, stream=True)

        self._save_response_content(response, destination)

    def _get_confirm_token(self, response):
        for key, value in response.cookies.items():
            if key.startswith('download_warning'):
                return value

        return None

    def _save_response_content(self, response, destination):
        CHUNK_SIZE = 32768

        with open(destination, "wb") as f:
            for chunk in response.iter_content(CHUNK_SIZE):
                if chunk:  # filter out keep-alive new chunks
                    f.write(chunk)