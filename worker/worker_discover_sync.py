# -*- encoding : utf-8 -*-
# sys libs
from logging import getLogger
from threading import Thread

# project modules
from cloud import dispatcher as cloud_sync
from worker.scan_stack import discovered_beacons_stack


class DiscoveredBeaconsSyncThread(Thread):

    def __init__(self):
        getLogger().debug('Discovered beacon thread created.')
        Thread.__init__(self)


    def run(self) -> None:
        getLogger().debug('Discovered beacon thread running.')
        while True:
            self._process_discovered_stack()


    def _process_discovered_stack(self):
        discovered_beacon = discovered_beacons_stack.get(block=True, timeout=None)
        discovered_beacons_stack.task_done()
        cloud_sync.send_beacon_discovered_notification(discovered_beacon.device.addr)