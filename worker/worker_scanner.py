# -*- encoding:utf-8 -*-
# sys modules
import time
import datetime
from threading import Thread
from logging import getLogger
# 3rd modules
from bluepy.btle import Scanner, DefaultDelegate
#project modules
import app_settings
from beacon.discoveredbeacon import DiscoveredBeacon
from beacon.parkedbeacon import ParkedBeacon
from helper.datetime.dateoperation import date_diff_in_seconds
from worker.scan_stack import parked_beacons_stack, discovered_beacons_stack, crowding_beacons_stack
from beacon.parser.beacon_parser import BeaconParser


class WorkerScannerThread(Thread):

    class ScanDelegate(DefaultDelegate):
        """Classe para obter os resultados do processamento do scanner"""

        def __init__(self, parent):
            DefaultDelegate.__init__(self)
            self._parent = parent

        def handleDiscovery(self, dev, is_new_dev, is_new_data):
            if is_new_dev:
                getLogger().debug("Discovered device: {0}".format(dev.addr))
            elif is_new_data:
                getLogger().debug("Received new data from {0}".format(dev.addr))
            else:
                getLogger().debug("Delegate empty")

            if BeaconParser.is_auth(dev.addr):
                getLogger().debug('AUTHORIZED beacon found: {}'.format(dev.addr))
                if BeaconParser.is_in_rssi_range(dev.addr, dev.rssi):
                    getLogger().info('Beacon RSSI {} in authz range.'.format(dev.rssi))
                    if dev.addr in self._parent.beacons_cache:
                        self._parent.beacons_cache[dev.addr].datetime_last_seen = datetime.datetime.now()
                    else:
                        # new beacon for monitoring parked time
                        parked_b = ParkedBeacon(dev)
                        parked_b.datetime_first_seen = datetime.datetime.now()
                        self._parent.beacons_cache[dev.addr] = parked_b

                        # notify new beacon presence
                        discover_beacon = DiscoveredBeacon(dev)
                        discovered_beacons_stack.put(discover_beacon)
                else:
                    getLogger().info('Beacon RSSI {} out of range. Don\'t process'.format(dev.rssi))
            else:
                getLogger().debug('UNAUTHORIZED beacon found: {}'.format(dev.addr))

    def __init__(self, scan_timeout=5, crowd_sync_seconds=60):
        """
        Cria uma instância de BeaconScannerThread para ficar monitorando os beacons em segundo plano.

        :param scan_timeout: Timeout do scan. A thread irá monitorar durante este tempo, ao final, irá processar
        os devices encontrados e logo em seguida iniciar um novo scan com o mesmo timeout.
        """
        Thread.__init__(self)
        self._scan_duration = scan_timeout
        self._crowd_sync_seconds = crowd_sync_seconds
        self._last_total_of_beacons_crowd = 0
        self.beacons_cache = {}


    @property
    def scan_duration(self):
        """
        Obtém o valor do timeout de cada scan.
        :return: Valor do timeout configurado.
        """
        return self._scan_duration

    @scan_duration.setter
    def scan_duration(self, value):
        """
        Configura um novo valor de timeout de scan
        :param value: Novo valor para timeout do scan
        :return: None
        """
        if value <= 0:
            self._scan_duration = 5
        else:
            self._scan_duration = value


    def run(self):
        """
        Ponto de entrada para execução da Thread.
        :return: None
        """
        try:
            getLogger().info("Creating scanner")
            scan_delegate = self.ScanDelegate(self)
            scanner = Scanner().withDelegate(scan_delegate)

            getLogger().info("Scanner created. Starting...")
            getLogger().info("Scanner started")
        except Exception as error:
            getLogger().error("Error creating scanner... Thread scanning dying...ERROR: {0}".format(error))
            return None

        while True:
            try:
                getLogger().debug('Starting new scan. Duration = {} s'.format(self._scan_duration))
                scanner.scan(self._scan_duration)
                devices = scanner.getDevices()
            except Exception as error:
                getLogger().error('Error while scanning for ble devices: {}'.format(error))
                break
            else:
                getLogger().debug('Devices found by scanner thread: {}'.format(len(devices)))
                if devices:
                    self._update_last_scan_file(devices=devices)

            # a cada scan processa o cache para verificar se tem algum beacon que deve ser adicionado a fila de envio
            self._process_discovered_cache()
            self._process_beacons_crowding()

            time.sleep(0.1)  # sleep for 100 ms before another scanning to prevent overloading

        scanner.stop()


    def _process_discovered_cache(self):
        """
         Processa o cache de beacons encontrados durante o scanning
         :return: None
         """
        keys_for_del = []
        for key, item in self.beacons_cache.items():
            # device já foi visto mais de uma vez
            if item.datetime_first_seen is not None and item.datetime_last_seen is not None:
                # determinar se já passou o timeout desde a última vez que o beacon foi encontrado pelo scan
                current_datetime = datetime.datetime.now()
                # último registro da lista, segundo index é posição do timestamp no registro
                last_datetime = item.datetime_last_seen
                # calculo da diferença
                diff_time_secs = date_diff_in_seconds(last_datetime, current_datetime)

                # verifica se já ultrapassou o timeout configurado no arquivo
                if diff_time_secs >= app_settings.app_config.beacon_exit_delay:
                    parked_beacons_stack.put(item)
                    # armazena a chave que deverá ser removida do cache, beacon já processado
                    keys_for_del.append(key)

            elif item.datetime_first_seen is not None and item.datetime_last_seen is None: # Visto somente uma vez
                current_datetime = datetime.datetime.now()
                last_seen_datetime = item.datetime_first_seen
                diff_time_secs = date_diff_in_seconds(last_seen_datetime, current_datetime)
                # se ja passou o timeout e foi visto somente uma vez, é porque o beacon
                # passou muito rapido e nao foi possivel capturar o tempo que ele ficou parado
                # embaixo da antena

                if diff_time_secs >= app_settings.app_config.beacon_exit_delay:
                    keys_for_del.append(key)

        # o que foi processado deve ser removido do cache
        for k in keys_for_del:
            del self.beacons_cache[k]



    def _process_beacons_crowding(self):
        # se houve alteração no tamanho da aglomeração, notifica o valor
        if len(self.beacons_cache) != self._last_total_of_beacons_crowd:
            self._last_total_of_beacons_crowd = len(self.beacons_cache)
            crowding_beacons_stack.put(self._last_total_of_beacons_crowd)


    @staticmethod
    def _update_last_scan_file(devices=None):
        """
        Atualiza o arquivo que exibe os beacons encontrados durante o último scan
        :param devices: Lista de devices do scan
        :return: None
        """
        try:
            with open(app_settings.LAST_SCAN_FILE_PATH, 'w') as myfile:
                current_dt = datetime.datetime.now().strftime('%d-%m-%Y %H:%M:%S')
                myfile.write('Last scan: ' + current_dt + '\n')
                header = 'MAC'.ljust(20) + 'RSSI'.ljust(7)
                myfile.write(header + '\n')
                if devices:
                    for dev in devices:
                        line = str(dev.addr).ljust(20) + str(dev.rssi).ljust(7)
                        myfile.write(line + '\n')
                else:
                    myfile.write('0 devices found!')

        except Exception as error:
            getLogger().debug('Error saving last scanned devices. Error {}'.format(error))
