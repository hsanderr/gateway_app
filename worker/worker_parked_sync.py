# -*- encoding : utf-8 -*-
# sys libs
import datetime
import time
from logging import getLogger
from threading import Thread

# project modules
from beacon.parser.beacon_parser import BeaconParser
from cloud import dispatcher as cloud_sync
from worker.scan_stack import parked_beacons_stack


class ParkedBeaconSyncThread(Thread):
    """
    Thread para ler a pilha de resultados dos beacons parados sob a antena e realizar o envio dos dados para nuvem
    """

    def __init__(self):
        Thread.__init__(self)
        self._beacon_temperature_cache = {}
        self._beacon_power_level_cache = {}
        self._last_timestamp_parked_processed = datetime.datetime.now()


    def run(self) -> None:
        while True:
            self.__process_parked_stack()


    def __process_parked_stack(self):
        """
         Processa a fila de de beacons parados na antena
         :return: None
         """
        # remove um elemento da lista, ou espera até que um item esteja disponível para ser obtido
        parked_beacon = parked_beacons_stack.get(block=True, timeout=None)
        parked_beacons_stack.task_done()
        self.__process_beacon_data(parked_beacon)


    def __process_beacon_data(self, parked_beacon):
        beacon = parked_beacon.device
        parked_time = parked_beacon.parked_time

        parked_time = str(datetime.timedelta(seconds=round(parked_time)))

        # envio de tempo parado
        cloud_sync.send_beacon_parked_time(beacon.addr, parked_time)

        # envio de nível da bateria
        self.__send_beacon_power_level(beacon)

        # envio de temperatura
        send_temp = BeaconParser.is_temp_enabled(beacon.addr)
        if send_temp:
            self.__send_beacon_temperature(beacon)
        else:
            getLogger().info('Sync temperature for beacon {} DISABLED'.format(beacon.addr))



    def __send_beacon_power_level(self, beacon):
        try:
            _, _, _, bat_level = self.__get_beacon_data(beacon)
        except Exception as error:
            getLogger().info("Error getting power level of beacon: {}. Error: {}", beacon.addr, error)
            bat_level = None

        if bat_level:
            if beacon.addr in self._beacon_power_level_cache:
                if self._beacon_power_level_cache[beacon.addr] != bat_level:
                    cloud_sync.send_beacon_power_level(beacon.addr, bat_level)
            else:
                self._beacon_power_level_cache[beacon.addr] = bat_level



    def __send_beacon_temperature(self, beacon):
        try:
            beacon_id, random_number, temp, bat_level = self.__get_beacon_data(beacon)
        except Exception as error:
            getLogger().info("Error getting data of beacon: {}. Error: {}", beacon.addr, error)
            beacon_id = None

        if beacon_id:
            last_random_number = self._beacon_temperature_cache.get(beacon.addr, 0)
            if random_number != last_random_number:
                getLogger().info('Sync temperature for beacon {} ENABLED'.format(beacon.addr))
                try:
                    self._beacon_temperature_cache[beacon.addr] = random_number
                    cloud_sync.send_beacon_temperature(beacon.addr, temp)
                except Exception as err:
                    getLogger().warning('Error sending temp. {}'.format(err))



    def __get_beacon_data(self, device):
        try:
            data = device.getScanData()[1][2]
            temp = data[6:8]
            temp = int(temp, 16)
            temp = (temp - 50) / 2

            bat_volt = data[8:]
            bat_volt = int(bat_volt, 16)
            bat_volt = (bat_volt * 3.6) / 255

            beacon_id = data[0:4]
            random_number = data[4:6]
        except Exception as error:
            getLogger().warning("Error parsing payload for beacon {}. Error: {}".format(device, error))
            return None, None, None, None
        else:
            return beacon_id, random_number, temp, bat_volt