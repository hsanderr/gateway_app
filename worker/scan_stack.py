# -*- encoding : utf-8 -*-
import queue

# Stack to store scan results for parked beacons
parked_beacons_stack = queue.Queue(50)

# Stack to notify beacons discovered
discovered_beacons_stack = queue.Queue(50)

# Stack to notify crowding values
crowding_beacons_stack = queue.Queue(5)