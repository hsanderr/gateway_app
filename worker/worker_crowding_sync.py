# -*- encoding : utf-8 -*-
# sys libs
import datetime
import time
from logging import getLogger
from threading import Thread

# project modules
from cloud import dispatcher as cloud_sync
from worker.scan_stack import crowding_beacons_stack

class CrowdingSyncThread(Thread):

    def __init__(self):
        Thread.__init__(self)


    def run(self) -> None:
        while True:
            crowding_value = crowding_beacons_stack.get(block=True, timeout=None)
            crowding_beacons_stack.task_done()
            cloud_sync.send_beacon_crowding(crowding_value)
