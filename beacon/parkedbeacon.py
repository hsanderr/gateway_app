from helper.datetime.dateoperation import date_diff_in_seconds

class ParkedBeacon:
    """
    Classe para informar monitorar o temepo de exposição do beacon na antena
    """
    def __init__(self, device = None):
        self.device = device
        self.datetime_first_seen = None
        self.datetime_last_seen = None

    @property
    def parked_time(self):
        if self.datetime_first_seen is not None and self.datetime_last_seen is not None:
            diff_secs = date_diff_in_seconds(self.datetime_first_seen, self.datetime_last_seen)
            return diff_secs
        else:
            return None
