# -*- encoding: utf-8 -*-
from beacon.device.generic.generic_beacon import GenericBeacon


class HedroBeaconGeneric(GenericBeacon):
    """
    Classe genérica para representar um beacon da Hedro. Possui capacidade para
    fornecer apenas MAC, RSSI e os dados do PAYLOAD.
    """
    def __init__(self, mac=None, rssi=None, data=None):
        GenericBeacon.__init__(self, mac=mac, rssi=rssi, data=data)

    @property
    def battery_level(self):
        raise NotImplementedError('Função não implementada.')


class HedroBeaconTemperature(HedroBeaconGeneric):
    """
    Classe para representar um beacon da Hedro que possui sensor para monitorar temperatura.
    """
    def __init__(self, mac=None, rssi=None, data=None):
        HedroBeaconGeneric.__init__(self, mac=mac, rssi=rssi, data=data)

    @property
    def temperature(self):
        raise NotImplementedError('Função não implementada.')


class HedroBeaconAccelerometer(HedroBeaconGeneric):
    """
    Classe para representar um beacon da Hedro que possui acelerômetro.
    """
    def __init__(self, mac=None, rssi=None, data=None):
        HedroBeaconGeneric.__init__(self, mac=mac, rssi=rssi, data=data)

    @property
    def x_axis_min_value(self):
        raise NotImplementedError('Função não implementada.')

    @property
    def x_axis_mean_value(self):
        raise NotImplementedError('Função não implementada.')

    @property
    def x_axis_max_value(self):
        raise NotImplementedError('Função não implementada.')

    @property
    def y_axis_min_value(self):
        raise NotImplementedError('Função não implementada.')

    @property
    def y_axis_mean_value(self):
        raise NotImplementedError('Função não implementada.')

    @property
    def y_axis_max_value(self):
        raise NotImplementedError('Função não implementada.')

    @property
    def z_axis_min_value(self):
        raise NotImplementedError('Função não implementada.')

    @property
    def z_axis_mean_value(self):
        raise NotImplementedError('Função não implementada.')

    @property
    def z_axis_max_value(self):
        raise NotImplementedError('Função não implementada.')


class HedroBeaconFFT(HedroBeaconGeneric):
    """
    Classe para representar beacon da hedro que realiza FFT.
    """
    def __init__(self, mac=None, rssi=None, data=None):
        HedroBeaconGeneric.__init__(self, mac=mac, rssi=rssi, data=data)

    @property
    def x_axis_fft(self):
        raise NotImplementedError('Função não implementada.')

    @property
    def y_axis_fft(self):
        raise NotImplementedError('Função não implementada.')

    @property
    def z_axis_fft(self):
        raise NotImplementedError('Função não implementada.')
