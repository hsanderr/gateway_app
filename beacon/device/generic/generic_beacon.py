# -*- encoding: utf-8 -*-


class GenericBeacon(object):
    """Beacon genérico. Identifica apenas o MAC e RSSI.
    """

    def __init__(self, mac=None, rssi=None, data=None):
        self._mac = mac
        self._rssi = rssi
        self._data = data

    @property
    def mac(self):
        return self._mac

    @mac.setter
    def mac(self, val):
        self._mac = val

    @property
    def rssi(self):
        return self._rssi

    @rssi.setter
    def rssi(self, val):
        self._rssi = val

    @property
    def data(self):
        return self._data
    
    @data.setter
    def data(self, new_data):
        self._data = new_data

    def get_vars_dict(self):
        beacon_vars = {'mac': self._mac, 'rssi': self._rssi}
        return beacon_vars
