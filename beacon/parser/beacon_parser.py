# -*- encoding:utf-8 -*-
from app_settings import MAC_AUTHZ_FILE_PATH

class BeaconParser(object):

    @staticmethod
    def is_auth(mac_addr):
        """
        Verifica se o mac informado está presente na lista de beacons autorizados para este gateway

        :param mac_addr: MAC do beacon que deseja verificar
        :return: True se o mac informado estiver na lista. False caso contrário.
        """
        mac = str(mac_addr).replace(':', '-').lower()
        with open(MAC_AUTHZ_FILE_PATH) as mac_file:
            for line in mac_file:
                line = line.replace(':', '-').lower()
                if mac in line:
                    return True

        return False

    @staticmethod
    def is_in_rssi_range(mac_addr, rssi):
        mac = str(mac_addr).replace(':', '-').lower()
        with open(MAC_AUTHZ_FILE_PATH) as mac_file:
            mac_file.readline()  # ignora primeira linha
            for line in mac_file:
                line = line.replace(',', ';')
                splited = line.split(';')
                rssi_index = 3
                line_mac = splited[0].replace(':', '-').lower()
                if line_mac == mac:
                    file_rssi = int(splited[rssi_index])
                    if rssi >= file_rssi:
                        return True

        return False

    @staticmethod
    def is_temp_enabled(mac_addr):
        """
        Verifica se a flag de temperatura está habilitada para o beacon no arquivo de autorização
        :param mac_addr: MAC do beacon que deseja verificar
        :return: True se estiver habilitado. False se não estiver habilitado ou se o beacon não estiver na lista
        """
        mac = str(mac_addr).replace(':', '-').lower()
        with open(MAC_AUTHZ_FILE_PATH) as mac_file:
            mac_file.readline() # ignora primeira linha
            for line in mac_file:
                line = line.replace(',', ';')
                splited = line.split(';')
                line_mac = splited[0].replace(':', '-').lower()
                if line_mac == mac:
                    if splited[1].lower() == 'y' or splited[1].lower() == '1':
                        return True

        return False

    @staticmethod
    def is_location_enabled(mac_addr):
        """
        Verifica se deve enviar a localização do beacon encontrado
        :param mac_addr: MAC do beacon que deseja verificar
        :return: True se estiver habilitado. False se não estiver habilitado ou se o beacon não estiver na lista.
        """
        mac = str(mac_addr).replace(':', '-').lower()
        with open(MAC_AUTHZ_FILE_PATH) as mac_file:
            mac_file.readline()  # ignora primeira linha
            for line in mac_file:
                line = line.replace(',', ';')
                splited = line.split(';')
                line_mac = splited[0].replace(':', '-').lower()
                if line_mac == mac:
                    if splited[2].lower() == 'y' or splited[2].lower() == '1':
                        return True

        return False
