# -*- encoding: utf-8 -*-
from logging import getLogger
import logging
import os
from configparser import ConfigParser
from urlfilecfg import UrlCfgFile

class AppSettings(object):
    """Classe de configuração do aplicativo.
    """

    def __init__(self, settings_file_path):
        super().__init__()
        self._settings_file_path = settings_file_path
        self._config_file = ConfigParser()
        self._config_file.read(self._settings_file_path)


    def get_sections(self):
        return self._config_file.keys()


    def get_keys(self, section):
        return self._config_file[section].keys()


    def gateway_token(self):
        """Obtém o token para envio das variáveis.
        
        Returns:
            str -- Token de autenticação das variáveis. Retorna False se algum erro ocorrer.
        """
        try:
            return self._config_file['gateway']['isense-token']
        except Exception as error:
            getLogger().warning('Error getting gateway token: {}'.format(error))
            return False

    def heatmap_position(self):
        """Obtém as posições x, y para post das variáveis de heatmap.
        
        Returns:
            float, float -- Tupla com os valores x, y da posição do gateway. Retorna False
            se algum erro ocorrer.
        """

        try:
            x_pos = self._config_file['gateway']['heatmap-x-axis-position']
            y_pos = self._config_file['gateway']['heatmap-y-axis-position']
            return float(x_pos), float(y_pos)
        except Exception as error:
            getLogger().warning('Error getting gateway heatmap position: {}'.format(error))
            return 0, 0


    @property
    def scan_duration(self):
        """
        Obtém o valor de timeout de duração de cada scan
        :return: Tempo em segundos de duração do timeout. Retorna 5.0 se ocorrer algum erro ao obter o valor.
        """
        try:
            timeout = self._config_file['gateway']['scan-duration']
            return float(timeout)
        except Exception as error:
            getLogger().warning('Error getting scan duration value. Returning default value: 5.0. Error: {}'.format(error))
            return 5.0

    @property
    def beacon_exit_delay(self):
        """
        Obtém o valor de timeout para monitoramento de beacon parado embaixo da antena
        :return:
        """
        try:
            monitor_timeout = self._config_file['gateway']['exit-delay']
            return float(monitor_timeout)
        except Exception as error:
            getLogger().warning(
                'Error getting beacon exit delay value. Returning default: 23. ERROR: {}'.format(error))
            return 23


# Global variable for sharing settings stored in file
ROOT_DIR = os.path.dirname(os.path.abspath(__file__))

# paths for files
CONFIG_FILE_PATH = os.path.join(ROOT_DIR, 'settings.cfg')
URLS_FILE_PATH = os.path.join(ROOT_DIR, 'urls.cfg')
LAST_SCAN_FILE_PATH = os.path.join(ROOT_DIR, 'last_scan.ini')
MAC_AUTHZ_FILE_PATH = os.path.join(ROOT_DIR, 'mac_authz.csv')

# objects with settings
app_config = AppSettings(CONFIG_FILE_PATH)
urls_config = UrlCfgFile(URLS_FILE_PATH)

LOG_PATH = os.path.join(ROOT_DIR, 'application.log')
RELEASE_DATE = '2019-11-29'
APP_VERSION = '2.2.0'


def init_logger():
    """
    Configura o logger do sistema.
    :return: None
    """
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    # arquivo
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    fh = logging.FileHandler(LOG_PATH)
    fh.setLevel(logging.WARNING)
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    # tela
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(formatter)
    logger.addHandler(ch)
