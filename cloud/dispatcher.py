#! -*- encoding: utf-8 -*-
# sys libs
from logging import getLogger

# 3rd libs
import requests

#project modules
from app_settings import app_config
from app_settings import urls_config
from helper.hardware import get_hardware_mac


def send_beacon_power_level(mac, power_level):
    """
    Envia o valor da bateria para nuvem
    :param mac: Mac do beacon
    :param power_level: Nível de bateria do beacon
    :return:
    """
    gateway_mac = get_hardware_mac()
    if gateway_mac:
        gateway_mac = str(gateway_mac).replace(':', '_')
    else:
        return None

    beacon_mac = str(mac).replace(':', '_')
    xpos, ypos = app_config.heatmap_position()

    if gateway_mac and beacon_mac:
        data = {'gateway_mac': gateway_mac,
                'beacon_mac': beacon_mac,
                'beacon_power_level':power_level,
                'xpos': xpos, 'ypos': ypos}
        header = {'Device-Token': app_config.gateway_token()}

        try:
            result = requests.post(urls_config.url_to_send_power_level, json=data, headers=header)
            return result.status_code
        except Exception as error:
            getLogger().warning('Exception sending beacon {} power level. {}'.format(mac, error))
            return None


def send_beacon_temperature(mac, temperature):
    """
    Envia o valor da temperatura do beacon para nuvem
    :param mac: Mac do beacon
    :param temperature: Temperatura do beacon
    :return:
    """

    gateway_mac = get_hardware_mac()
    if gateway_mac:
        gateway_mac = str(gateway_mac).replace(':', '_')
    else:
        return None

    beacon_mac = str(mac).replace(':', '_')
    xpos, ypos = app_config.heatmap_position()

    if gateway_mac and beacon_mac:
        data = {'gateway_mac': gateway_mac,
                'beacon_mac': beacon_mac,
                'beacon_temperature': temperature,
                'xpos': xpos, 'ypos': ypos}
        header = {'Device-Token': app_config.gateway_token()}

        try:
            result = requests.post(urls_config.url_to_send_temperature, json=data, headers=header)
            return result.status_code
        except Exception as error:
            getLogger().warning('Exception sending beacon {} temperature. {}'.format(mac, error))
            return None


def send_beacon_discovered_notification(mac):
    """
    Notifica nuvem sobre passagem do beacon pela atena
    :param mac: Mac do beacon
    :return:
    """

    gateway_mac = get_hardware_mac()
    if gateway_mac:
        gateway_mac = str(gateway_mac).replace(':', '_')
    else:
        return None

    beacon_mac = str(mac).replace(':', '_')
    xpos, ypos = app_config.heatmap_position()

    if gateway_mac and beacon_mac:
        data = {'gateway_mac': gateway_mac,
                'beacon_mac': beacon_mac,
                'xpos': xpos, 'ypos': ypos}
        header = {'Device-Token': app_config.gateway_token()}

        try:
            result = requests.post(urls_config.url_to_notify_beacon_discovered, json=data, headers=header)
            return result.status_code
        except Exception as error:
            getLogger().warning('Exception sending discovered beacon {}. {}'.format(mac, error))
            return None


def send_beacon_parked_time(mac, parked_time):
    """
    Envia para nuvem o tempo que um beacon ficou parado na antena.
    :param mac: Mac do beacon
    :param parked_time: Tempo de parada do beacon
    :return:
    """
    gateway_mac = get_hardware_mac()
    if gateway_mac:
        gateway_mac = str(gateway_mac).replace(':', '_')
    else:
        return None

    beacon_mac = str(mac).replace(':', '_')
    xpos, ypos = app_config.heatmap_position()

    if gateway_mac and beacon_mac:
        data = {'gateway_mac': gateway_mac,
                'beacon_mac': beacon_mac,
                'beacon_stopped_time': parked_time,
                'xpos': xpos, 'ypos': ypos}
        header = {'Device-Token': app_config.gateway_token()}

        try:
            result = requests.post(urls_config.url_to_notify_beacon_parked_time, json=data, headers=header)
            return result.status_code
        except Exception as error:
            getLogger().warning('Exception sending beacon {} parked time. {}'.format(mac, error))
            return None


def send_beacon_crowding(crowding_value):
    """
    Envia para nuvem o valor da quantidade de beacons parado sob a antena
    :param crowding_value: Quantidade de beacons parados sob antena
    :return:
    """
    gateway_mac = get_hardware_mac()
    if gateway_mac:
        gateway_mac = str(gateway_mac).replace(':', '_')
        xpos, ypos = app_config.heatmap_position()

        header = {'Device-Token': app_config.gateway_token()}
        data = {'gateway_mac': gateway_mac,
                'crowding': crowding_value,
                'xpos': xpos, 'ypos': ypos}

        try:
            result = requests.post(urls_config.url_to_send_crowding, json=data, headers=header)
            return result.status_code
        except Exception as error:
            getLogger().warning('Exception sending crowding value. {}'.format(error))
            return None