# -*- encoding: utf-8 -*-
# sys modules
import os
import sys
import time
import logging

if sys.version_info < (3, 0):
    print('This application must be executed with python3')
    exit(1)

# project modules
import app_settings
from worker.worker_scanner import WorkerScannerThread
from worker.worker_parked_sync import ParkedBeaconSyncThread
from worker.worker_discover_sync import DiscoveredBeaconsSyncThread
from worker.worker_crowding_sync import CrowdingSyncThread

# variáveis de threads
thread_scanner = None
thread_sync_parked_beacons = None
thread_sync_discovered_beacons = None
thread_sync_crowding = None

def run():
    # criação de todas as threads
    start_thread_scanner()
    start_thread_parked_sync()
    start_thread_discovered_sync()
    start_thread_crowding_sync()

    # Thread principal rodando forever
    while True:
        # verificando se as threads estão disponíveis e em execução
        check_if_thread_scanner_is_alive()
        check_if_thread_parked_sync_is_alive()
        check_if_thread_discovered_sync_is_alive()
        check_if_thread_crowding_sync_is_alive()

        # coloca Thread principal para dormir por um tempo para verificar novamente se as outras estão rodando
        main_sleep_time = 15.0
        logging.getLogger().debug('Main thread sleeping for {} seconds'.format(main_sleep_time))
        time.sleep(main_sleep_time)


def start_thread_scanner():
    global thread_scanner

    timeout = app_settings.app_config.scan_duration
    thread_scanner = WorkerScannerThread(scan_timeout=timeout)
    logging.getLogger().debug('Starting thread worker')
    thread_scanner.start()


def start_thread_parked_sync():
    global thread_sync_parked_beacons

    thread_sync_parked_beacons = ParkedBeaconSyncThread()
    logging.getLogger().debug('Starting thread to sync data of parked beacons')
    thread_sync_parked_beacons.start()


def start_thread_discovered_sync():
    global thread_sync_discovered_beacons

    thread_sync_discovered_beacons = DiscoveredBeaconsSyncThread()
    logging.getLogger().debug('Starting thread to sync discovered beacons')
    thread_sync_discovered_beacons.start()


def start_thread_crowding_sync():
    global thread_sync_crowding

    thread_sync_crowding = CrowdingSyncThread()
    logging.getLogger().debug('Starting thread to sync crowding values')
    thread_sync_crowding.start()


def check_if_thread_scanner_is_alive():
    global  thread_scanner

    if not thread_scanner or not thread_scanner.isAlive():
        logging.getLogger().warning('Thread to scan beacons is dead. Restarting...')
        try:
            thread_scanner = WorkerScannerThread(scan_timeout=app_settings.app_config.scan_duration)
            thread_scanner.start()
        except Exception as error:
            logging.getLogger().warning('Error restarting scanner thread: {}'.format(error))


def check_if_thread_parked_sync_is_alive():
    global thread_sync_parked_beacons

    if not thread_sync_parked_beacons or not thread_sync_parked_beacons.isAlive():
        logging.getLogger().warning('Thread to sync data is dead. Restarting...')
        try:
            thread_sync_parked_beacons = ParkedBeaconSyncThread()
            thread_sync_parked_beacons.start()
        except Exception as error:
            logging.getLogger().warning('Error restarting worker sync thread: {}'.format(error))


def check_if_thread_discovered_sync_is_alive():
    global  thread_sync_discovered_beacons

    if not thread_sync_discovered_beacons or not thread_sync_discovered_beacons.isAlive():
        logging.getLogger().warning('Thread do sync discovered beacons is dead. Restarting...')
        try:
            thread_sync_discovered_beacons = DiscoveredBeaconsSyncThread()
            thread_sync_discovered_beacons.start()
        except Exception as error:
            logging.getLogger().warning('Error restarting thread to sync discovered beacons. {}'.format(error))


def check_if_thread_crowding_sync_is_alive():
    global thread_sync_crowding

    if not thread_sync_crowding or not thread_sync_crowding.isAlive():
        logging.getLogger().warning('Thread to sync crowding values is dead. Restarting...')
        try:
            thread_sync_crowding = CrowdingSyncThread()
            thread_sync_crowding.start()
        except Exception as error:
            logging.getLogger().warning('Exception restarting thread to sync crowding beacons. {}'.format(error))


def check_if_running_as_root():
    if os.getuid() != 0:
        logging.getLogger().warning("This application must be started as root using [sudo] command.\ne.g.: sudo python3 main.py")
        exit(1)


if __name__ == '__main__':
    app_settings.init_logger()

    check_if_running_as_root()

    logging.getLogger().warning(
        'Booting application. Version: {}. Release date: {}'.format(app_settings.APP_VERSION, app_settings.RELEASE_DATE))

    # configurando timezone da aplicação
    os.environ['TZ'] = 'America/Sao_Paulo'
    time.tzset()

    # Execução da aplicação principal
    run()
