from logging import getLogger
from configparser import ConfigParser

class UrlCfgFile:


    def __init__(self, filepath = None):
        self.filepath = filepath
        self._config_file = ConfigParser()
        self._config_file.read(self.filepath)


    @property
    def sections(self):
        """Obtém todas as seções do arquivo de configuração de urls
         :return Lista de seções disponíveis
         """
        return self._config_file.keys()


    @property
    def keys(self, section):
        """
         Retorna todas as chaves da seção informada
         :param section: Seção para obter todas as chaves disponíveis
         :return: Lista com todas as chaves
         """
        return self._config_file[section].keys()


    @property
    def url_to_send_crowding(self):
        """
         Url para enviar o valor de aglomeração de beacons sob a antena.
         :return: str para url, ou False se ocorrer algum erro.
         """
        try:
            return self._config_file['urls']['url-crowding']
        except Exception as error:
            getLogger().warning('Error getting crowding url: {}'.format(error))
            return False


    @property
    def url_to_send_temperature(self):
        """
         Url para enviar o valor de temperatura informado pelo beacon
         :return: str para url, ou False se ocorrer algum erro.
         """
        try:
            return self._config_file['urls']['url-temperature']
        except Exception as error:
            getLogger().warning('Error getting temperature url: {}'.format(error))
            return False


    @property
    def url_to_send_power_level(self):
        """
         Url para enviar o valor da bateria informado pelo beacon
         :return: str para url, ou False se ocorrer algum erro.
         """
        try:
            return self._config_file['urls']['url-power-level']
        except Exception as error:
            getLogger().warning('Error getting power level url: {}'.format(error))
            return False


    @property
    def url_to_notify_beacon_discovered(self):
        """
         Url para notificar que um beacon acabou de passar pela antena
         :return: str para url, ou False se ocorrer algum erro.
         """
        try:
            return self._config_file['urls']['url-beacon-discovered']
        except Exception as error:
            getLogger().warning('Error getting beacon discovered url: {}'.format(error))
            return False

    
    @property
    def url_to_notify_beacon_parked_time(self):
        """
         Url para enviar o valor de tempo de parada do beacon sob a antena
         :return: str para url, ou False se ocorrer algum erro.
         """
        try:
            return self._config_file['urls']['url-parked-time']
        except Exception as error:
            getLogger().warning('Error getting beacon parked time url: {}'.format(error))
            return False